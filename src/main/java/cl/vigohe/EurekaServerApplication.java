package cl.vigohe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.commons.util.InetUtils;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaServer
@EnableDiscoveryClient
public class EurekaServerApplication {
    private static Logger logger = LoggerFactory.getLogger(EurekaServerApplication.class);

    @Value("${server.port}")
    private int port;

    @Value("${spring.application.name}")
    private String hostname;

	public static void main(String[] args) {
		SpringApplication.run(EurekaServerApplication.class, args);
	}

	@Bean
    @Profile({"eureka-server-01","eureka-server-02"})
    public EurekaInstanceConfigBean eurekaInstanceConfigBean(InetUtils inetUtils) {

        String ip = rancherIp();

        EurekaInstanceConfigBean config = new EurekaInstanceConfigBean(inetUtils);

		config.setHostname(hostname);
		config.setIpAddress(ip);
		config.setNonSecurePort(port);

		return config;
	}

	private String rancherIp(){
        final String uri = "http://rancher-metadata/latest/self/container/primary_ip";
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(uri, String.class);
    }
}
